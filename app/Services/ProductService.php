<?php

namespace App\Services;

use App\Services\ProductServiceInterface;

use App\Repositories\ProductRepositoryInterface;
use App\Repositories\CountryRepositoryInterface;
class ProductService implements ProductServiceInterface
{

    protected $productRepository;
    protected $CountryRepository;
    function __construct(ProductRepositoryInterface $productRepository,CountryRepositoryInterface $CountryRepository){
        $this->productRepository = $productRepository;
        $this->CountryRepository=$CountryRepository;
    }
public function createNewProduct($request){

    $request->validate([
        "name"=>'required',
        "price"=>'required|numeric',
        "weight"=>'required',
        "country"=>'required||exists:countries,name'

    ]);

    $data=$request->all();
   $product= $this->productRepository->createNew($data);







}

}
