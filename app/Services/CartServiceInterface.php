<?php

namespace App\Services;

interface CartServiceInterface
{

    public function calculateTotal();
    public function getAllProdut();
}
