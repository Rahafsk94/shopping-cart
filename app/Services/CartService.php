<?php

namespace App\Services;
use  App\Repositories\CartRepositoryInterface;
use  App\Repositories\CountryRepositoryInterface;
use App\Repositories\ProductRepositoryInterface;

class CartService implements CartServiceInterface
{
    protected $productRepository;

    protected $cartRepository;
    protected $countryRepository;
    public function __construct(ProductRepositoryInterface $productRepository, CartRepositoryInterface $cartRepository,CountryRepositoryInterface $countryRepository )

    {
        $this->productRepository = $productRepository;
        $this->cartRepository = $cartRepository;
        $this->countryRepository = $countryRepository;
    }

    public function getAllProdut()
    {
        return $this->cartRepository->getProductList();
    }

    public function removeProductById($id)
    {
        return $this->cartRepository->removeById($id);
    }
    public function updateProductById($id,$quantity)
    {

        return $this->cartRepository->updateQuantityById($id,$quantity);
    }

    public function addProduct($id, $name, $price, $quantity)
    {



        $pro=[
            'id' => $id,
            'name' => $name,
            'price' => $price,
            'quantity' => $quantity,
            'attributes' => array(
                'image' => null,
            )
            ];

        return $this->cartRepository->addToCart($pro);
    }

   public function calculateTotal()
    {
        $productList=$this->cartRepository->getCartList();

        $total=($this->getSubTotalWithVat('14') + $this->calculateShippingCost( $productList) ) - $this->calculateDiscounts($productList) ;
        return $total ;
    }


    public function getSubTotalWithVat($vat)
    {

      $subtotal = $this->cartRepository->getSubTotal();

      $subWithVat= $subtotal + (($subtotal * $vat)/100);


      return  $subWithVat ;
    }

    public function calculateShippingCost($productList){

           $shippingCostTotal=0;

            foreach ($productList as $item){


            $country=$this->productRepository->getCountry($item['id']);
             //weight in kg
            $productweigh=$this->productRepository->getById($item['id']);
            $quantity= $item['quantity'];
            $totalweight=$quantity * $productweigh;
            // total weight in g
            $totalweightInGram=$totalweight*1000;

            $rate=$this->countryRepository->getRateById($country);
             //the reate for each 100g
            $shippingCostTotal +=($totalweightInGram * $rate)/100;

        }


          return $shippingCostTotal;
    }

////
     function calculateDiscounts($productList) {
    $totalDiscount = 0;
    $shirtCount = 0;
    $blouseCount = 0;
    $jacketFound = false;
    $totalQuantity =0;


       // عرض الحذاء: خصم 10% إذا كان المنتج حذاء

    foreach ($productList as $item){

        $totalQuantity += $item['quantity'];

         if( $item['name']=='Shoes'){
            $shoeDiscount = $item['price'] * $item['quantity'] * 0.1;

            $totalDiscount += $shoeDiscount;

         }

         if($item['name'] =='T-shirt'){
            $shirtCount += $item['quantity'];


         }
         elseif ($item['name']=='Blouse') {
            $blouseCount += $item['quantity'];

        } elseif ($item['name']== 'Jacket') {
            $jacketFound = true;
            $jackePrice=$item['price'];

        }


    }
//اذا كمية البضاعة اكبر من واحد خصم totalquantity
    if ($totalQuantity >= 2) {

        $shippingDiscount = 10;

        $totalDiscount += $shippingDiscount;
    }

        if ($shirtCount >= 2 ||  $blouseCount >= 2 || ($shirtCount >= 1 && $blouseCount >= 1)) {

        if ($jacketFound) {
            $totalDiscount =$totalDiscount +  ($jackePrice/2 );

            // حسم نصف سعر الجاكيت

        }

    }

    return $totalDiscount;

}

  public function  clearAllCart(){
    $this->cartRepository->clearAllCart();
  }
}
