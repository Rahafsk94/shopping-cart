<?php

namespace App\Repositories;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Models\Product;
use App\Models\Country;


class ProductRepository implements ProductRepositoryInterface
{

    public function getById($id)
    {
      $product = Product::find($id);
        return $product->weight;
    }



    public function getCountry($id){
        $product = Product::find($id);
        return $product->country_id;
    }

    public function getWeightId($id)
    {
      $product = Product::find($id);
        return $product->weight;
    }

    public function createNew($data){

$country=Country::where('name','=',$data['country'])->first();

       $product= new Product($data);
       $product->country()->associate($country);
       return  $product->save();
    }

}
