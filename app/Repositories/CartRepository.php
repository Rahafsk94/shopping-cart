<?php


namespace App\Repositories;

class CartRepository implements  CartRepositoryInterface
{

 public function  removeById($id){

        \Cart::remove($id);
    }

    public function getCartList()
    {

     return \Cart::getContent()->toArray();
    }



    public function getSubTotal()
    {
       return $subtotal =\Cart::getsubtotal() ;

    }

    public function getProductList(){
        return \Cart::getContent();
    }

    public function addToCart($productArray){

        return   \Cart::add($productArray);
    }
    public function updateQuantityById($id,$quantity){

        return \Cart::update($id,['quantity' => [
            'relative' => false,
            'value' => $quantity
        ],]);
    }
    public function clearAllCart(){
     return   \Cart::clear();
    }
}
