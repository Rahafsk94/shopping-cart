<?php


namespace App\Repositories;

interface CartRepositoryInterface
{

    public function getCartList();
    public function getSubTotal();
    public function getProductList();
    public function addToCart($productArray);


}

