<?php

namespace App\Repositories;

interface CountryRepositoryInterface
{
    public function getRateById($id);
}
