<?php

namespace App\Repositories;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Models\Country;
class CountryRepository implements CountryRepositoryInterface
{

    public function getRateById($id)
    {
        $country=Country::find($id);
         return $country->rate;
    }

    public function getByName($name)
    {

        $country=Country::where('name','=',$name)->first();
         return $country;
    }
}
