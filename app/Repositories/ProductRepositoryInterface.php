<?php

namespace App\Repositories;

interface ProductRepositoryInterface
{
    public function getById($id);
    public function getCountry($id);
}
