<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Services\CartServiceInterface;

class CartController extends Controller
{
    private $cartService;

    public function __construct(CartServiceInterface $cartService) {
        $this->cartService = $cartService;
    }

    public function cartList()
    {

      $cartItems=$this->cartService->getAllProdut();
      $total=$this->cartService->calculateTotal();

       return view('shopping.cart.cartlist', compact('cartItems','total'));
    }


    public function addToCart(Request $request)
    {

      $this->cartService->addProduct($request->id, $request->name, $request->price, $request->quantity);

        session()->flash('success', 'Product is Added to Cart Successfully !');

        return redirect()->route('cart.list');
    }



    public function removeProductFromCart(Request $request){

        $this->cartService->removeProductById($request->id);
        session()->flash('success', 'Item Cart Remove Successfully !');

        return redirect()->route('cart.list');


    }

    public function updateCart(Request $request)
    {

        $this->cartService->updateProductById($request->id,$request->quantity);

        session()->flash('success', 'Item Cart is Updated Successfully !');

        return redirect()->route('cart.list');
    }

    public function clearAllCart()
    {
        $this->cartService->clearAllCart();


        session()->flash('success', 'All Item Cart Clear Successfully !');

        return redirect()->route('cart.list');
    }
/*
    public function getSubTotalWithVat($vat){

            $subtotal =\Cart::getsubtotal() ;
          $subWithVat= $subtotal + (($subtotal * $vat)/100);


return  $subWithVat ;

            // يمكنك عرض $subtotal أو استخدامه في محاسبة السعر الإجمالي

    }

*/

    /*public function calculateShipping($productWeight, $quantity, $rate) {
        return $productWeight * $quantity * $rate;
    }*/


//تكلفة الشحن
   /* public function shipping($cartItems){
        $shippingCostTotal=0;
        foreach ($cartItems as $item){

            $product=Product::find($item->id);
            $country=Country::find($product->country_id);

           $productW= $product->weight;//weight in kg

           $quantity= $item->quantity;
           $totalweight=$quantity * $productW;// total weight in kg
           $totalweightInGram=$totalweight*1000;


          $rate= $country->rate;
          $shippingCostTotal +=($totalweightInGram * $rate)/100;





        }

        return $shippingCostTotal;


    }
*/




   /* function calculateDiscounts($cartItems) {
        $totalDiscount = 0;
        $shirtCount = 0;
        $blouseCount = 0;
        $jacketFound = false;
        $totalQuantity =0;


           // عرض الحذاء: خصم 10% إذا كان المنتج حذاء

        foreach ($cartItems as $item){

            $totalQuantity += $item->quantity;
             if( $item->name=='Shoes'){
                $shoeDiscount = $item->price * $item->quantity * 0.1;

                $totalDiscount += $shoeDiscount;

             }

             if($item->name =='T-shirt'){
                $shirtCount += $item->quantity;
               // dd('hi');

             }
             elseif ($item->name=='Blouse') {
                $blouseCount += $item->quantity;

            } elseif ($item->name == 'Jacket') {
                $jacketFound = true;
                $jackePrice=$item->price;

            }


        }
//اذا كمية البضاعة اكبر من واحد خصم totalquantity
        if ($totalQuantity >= 2) {

            $shippingDiscount = 10;

            $totalDiscount += $shippingDiscount;
        }

            if ($shirtCount >= 2 ||  $blouseCount >= 2 || ($shirtCount >= 1 && $blouseCount >= 1)) {
            if ($jacketFound) {
                $totalDiscount =$totalDiscount +  ($jackePrice/2 );
//dd($totalDiscount);
               // $jacketDiscount = $item->price; // حسم نصف سعر الجاكيت

            }

        }
    //    dd( $totalDiscount);
        return $totalDiscount;

    }
*/
}
