<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Services\ProductServiceInterface;
//    طبقة ال كارت سيرفس تنظيف و التاكد من  فاليديشن و الرولز وين مكانا
class ProductController extends Controller
{
    private $productService;

    function __construct(ProductServiceInterface $productService){
        $this->productService=$productService;

    }
    public function productList()
    {
//$this->productService->createNewProduct();
        $products = Product::all();

        return view('shopping.products.index', compact('products'));
    }


    public function create()
    {
        return view('shopping.products.create');
    }

    public function store(Request $request)
    {
     $this->productService->createNewProduct($request);
    }


    public function show(Product $product)
    {
        //
    }


    public function edit(Product $product)
    {
        //
    }


    public function update(Request $request, Product $product)
    {
        //
    }


    public function destroy(Product $product)
    {
        //
    }
}
