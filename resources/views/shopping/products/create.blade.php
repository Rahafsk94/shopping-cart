@extends('shopping.layouts.productlayout')
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

    <div class="card cards" style="width: 50rem;">
        <div class="card-body">
          <h1>Create New Product</h1>


    <form action="{{route('products.store')}}" method="POST">
      @csrf
      <div class="column">
      <div class="form-group">
        <strong>Product Name:</strong>
        <input type="text" class="form-control" name="name">
      </div>
      <div class="form-group">
        <strong>Product image:</strong>
        <input type="file" class="form-control" name="image">
      </div>
    </div>
    <div class="column">
      <div class="form-group">
       <strong>Product Price:</strong>
        <input type="number" class="form-control" name="price">

      </div>
      <div class="form-group">
        <strong>Product weight:</strong>
        <input type="number" class="form-control" name="weight">
      </div>
      <div class="form-group">
        <strong>Country:</strong>
        <input type="text" class="form-control" name="country">
      </div>
      <div class="float-right">
      <button type="submit" class="btn btn-primary">Add Product</button>
    </div>
    </form>
    </div>
  </div>
  </div>



@endsection


