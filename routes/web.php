<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('products','ProductController@productList')->name('products.list');
Route::get('products/create','ProductController@create');
Route::post('products/store','ProductController@store')->name('products.store');
Route::get('cart', 'CartController@cartList')->name('cart.list');
Route::post('cart','CartController@addToCart')->name('cart.store');
Route::post('update-cart', 'CartController@updateCart')->name('cart.update');
Route::post('remove', 'CartController@removeProductFromCart')->name('cart.remove');
Route::post('clear', 'CartController@clearAllCart')->name('cart.clear');
